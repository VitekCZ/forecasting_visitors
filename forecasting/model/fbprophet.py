import datetime
import time
from sklearn.metrics import mean_squared_error, mean_absolute_error
from scipy.stats import boxcox
from fbprophet import Prophet
from pmdarima.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas import DataFrame
from pandas import to_datetime
from czech_holidays import Holidays


class Fbprophet:
    # pro instalaci fb prophet je nutne pouzit tyto odkazy
    # pip install pystan==2.17.1.0 -i https://pypi.tuna.tsinghua.edu.cn/simple
    # pip install fbprophet -i https://pypi.tuna.tsinghua.edu.cn/simple

    def __init__(self, name, timeserie, exogenous_var, holiday, weather):
        self.name = name
        self.timeserie = timeserie
        self.exogenous_var = exogenous_var
        self.holiday = holiday
        self.weather = weather

    def holidays(self, number):
        cz_holidays_imp = Holidays(number)
        column_names = ["holiday", "ds"]
        cz_holidays = pd.DataFrame(columns=column_names)

        cz_holidays_next = Holidays(number - 1)
        for holiday in cz_holidays_next:
            dt = pd.DataFrame({'holiday': holiday.name, 'ds': pd.to_datetime([datetime.date(
                holiday.year, holiday.month, holiday.day)])})
            cz_holidays = pd.concat([cz_holidays, dt])
        for holiday in cz_holidays_imp:
            dt = pd.DataFrame({'holiday': holiday.name, 'ds': pd.to_datetime([datetime.date(
                holiday.year, holiday.month, holiday.day)])})
            cz_holidays = pd.concat([cz_holidays, dt])
        cz_holidays_next = Holidays(number + 1)
        for holiday in cz_holidays_next:
            dt = pd.DataFrame({'holiday': holiday.name, 'ds': pd.to_datetime([datetime.date(
                holiday.year, holiday.month, holiday.day)])})
            cz_holidays = pd.concat([cz_holidays, dt])

        return cz_holidays

    def errors(self, forecast, test):
        mse = mean_squared_error(y_true=test,
                                 y_pred=forecast)
        print("MSE: ", mse)
        mae = mean_absolute_error(y_true=test,
                                  y_pred=forecast)
        print("MAE: ", mae)

    def predict_test(self):
        horizons_test = 7
        ts_visitors_day = self.timeserie.ts_visitors_day.loc[self.timeserie.min_date:self.timeserie.max_date]
        ts_exog_day = self.timeserie.ts_exog_weather_day.loc[
                      self.timeserie.min_date:self.timeserie.max_date - datetime.timedelta(days=1)]
        ts_visitors_day.dropna(inplace=True)

        index_forecasted = np.arange(len(ts_visitors_day.values))
        ts_visitors_day['Datum'] = ts_visitors_day.index
        ts_visitors_day['y'] = ts_visitors_day[self.timeserie.park]
        ts_visitors_day.drop(columns=[self.timeserie.park], inplace=True)
        ts_visitors_day.index = index_forecasted
        ts_visitors_day.columns = ['ds', 'y']

        train, test = train_test_split(ts_visitors_day, test_size=horizons_test)
        train_exog, test_exog = train_test_split(ts_exog_day, test_size=horizons_test)

        years = pd.DatetimeIndex(train['ds']).year
        if self.holiday:
            cz_holidays = self.holidays(max(years))

        if self.exogenous_var:
            pd.options.mode.chained_assignment = None
            train.loc[:, 'tmax'] = train_exog.loc[:, 'tmax'].values
            train.loc[:, 'tlak'] = train_exog.loc[:, 'tlak'].values
            train.loc[:, 'vlhkost'] = train_exog.loc[:, 'vlhkost'].values

        if self.holiday:
            model = Prophet(
                interval_width=0.95,
                seasonality_mode="additive",
                yearly_seasonality=True,
                weekly_seasonality=True,
                daily_seasonality=False,
                holidays_prior_scale=0.1,
                holidays=cz_holidays
            )
        else:
            model = Prophet(
                interval_width=0.95,
                seasonality_mode="additive",
                yearly_seasonality=True,
                weekly_seasonality=True,
                daily_seasonality=False
            )

        if self.weather:
            model.add_regressor('tmax')
            model.add_regressor('tlak')
            model.add_regressor('vlhkost')

        timestart = time.perf_counter()
        # fit the model
        if self.weather:
            model.fit(train.reset_index().rename(columns={'Datetime': 'ds',
                                                          'Podhura': 'y',
                                                          'tmax': 'tmax',
                                                          'tlak': 'tlak',
                                                          'vlhkost': 'vlhkost'
                                                          }))
        else:
            model.fit(train.reset_index().rename(columns={'Datetime': 'ds',
                                                          'Podhura': 'y',
                                                          }))

        timeend = time.perf_counter()
        print(f" {timeend - timestart:0.4f} vteřin")
        future = model.make_future_dataframe(periods=horizons_test, freq='D', include_history=True)
        future['floor'] = min(train['y'].values)

        if self.weather:
            exog = pd.concat([train_exog, test_exog])
            future.loc[:, 'tmax'] = exog.loc[:, 'tmax'].values
            future.loc[:, 'tlak'] = exog.loc[:, 'tlak'].values
            future.loc[:, 'vlhkost'] = exog.loc[:, 'vlhkost'].values

        fcst = model.predict(future)

        BOXCOX_LAMBDA = 1
        fcst["yhat"] = boxcox(fcst['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcst["yhat_lower"] = boxcox(fcst['yhat_lower'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcst["yhat_upper"] = boxcox(fcst['yhat_upper'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcst.index = fcst['ds']
        fig = model.plot(fcst)
        plt.show()

        # define the period for which we want a prediction
        horizons = horizons_test
        future = DataFrame(test['ds'])
        future.columns = ['ds']
        future['ds'] = to_datetime(future['ds'])
        future['floor'] = min(train['y'].values)
        if self.weather:
            future['tmax'] = test_exog['tmax'].values
            future['tlak'] = test_exog['tlak'].values
            future['vlhkost'] = test_exog['vlhkost'].values

        forecast = model.predict(future)

        model.plot_components(forecast)
        plt.title('Složky')
        plt.show()

        print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].head())

        forecast["yhat"] = boxcox(forecast['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)

        forecast["yhat_lower"] = forecast['yhat_lower'].clip(lower=0)
        forecast["yhat_upper"] = boxcox(forecast['yhat_upper'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        forecast.index = forecast['ds']
        train.index = train['ds']
        test.index = test['ds']

        plt.plot(train.tail(horizons * 4)['y'], c='blue', label='Trénovací data')
        plt.plot(test['y'], c='orange', label='Testovací data')
        plt.plot(forecast['yhat'], c='green', label='Predikce')
        plt.fill_between(forecast.index,
                         forecast['yhat_lower'],
                         forecast['yhat_upper'],
                         color='k', alpha=.15)
        self.errors(forecast['yhat'], test['y'])
        plt.legend()
        plt.title('Prophet - ' + self.timeserie.park + " test predikce")
        plt.xticks(rotation=20)
        plt.show()

    def predict(self):
        ts_visitors_day = self.timeserie.ts_visitors_day.copy()
        ts_visitors_day = self.timeserie.ts_visitors_day.loc[self.timeserie.min_date:self.timeserie.max_date]
        ts_exog_day = self.timeserie.ts_exog_weather_day.loc[
                      self.timeserie.min_date:self.timeserie.max_date - datetime.timedelta(days=1)]
        ts_exog_day_pred = self.timeserie.ts_exog_weather_day.loc[
                           self.timeserie.max_date:self.timeserie.max_predicted_date - datetime.timedelta(days=1)]
        ts_visitors_day.dropna(inplace=True)
        index_forecasted = np.arange(len(ts_visitors_day.values))
        ts_visitors_day['Datum'] = ts_visitors_day.index
        ts_visitors_day['y'] = ts_visitors_day[self.timeserie.park]
        ts_visitors_day.drop(columns=[self.timeserie.park], inplace=True)
        ts_visitors_day.index = index_forecasted
        ts_visitors_day.columns = ['ds', 'y']

        years = pd.DatetimeIndex(ts_visitors_day['ds']).year
        if self.holiday:
            cz_holidays = self.holidays(max(years))

        if self.weather:
            pd.options.mode.chained_assignment = None
            ts_visitors_day.loc[:, 'tmax'] = ts_exog_day.loc[:, 'tmax'].values
            ts_visitors_day.loc[:, 'tlak'] = ts_exog_day.loc[:, 'tlak'].values
            ts_visitors_day.loc[:, 'vlhkost'] = ts_exog_day.loc[:, 'vlhkost'].values

        if self.holiday:
            model = Prophet(
                interval_width=0.95,
                seasonality_mode="additive",
                yearly_seasonality=True,
                weekly_seasonality=True,
                daily_seasonality=False,
                holidays_prior_scale=0.1,
                holidays=cz_holidays
            )
        else:
            model = Prophet(
                interval_width=0.95,
                seasonality_mode="additive",
                yearly_seasonality=True,
                weekly_seasonality=True,
                daily_seasonality=False
            )

        if self.weather:
            model.add_regressor('tmax')
            model.add_regressor('tlak')
            model.add_regressor('vlhkost')
        # fit the model
        timestart = time.perf_counter()
        if self.weather:
            model.fit(ts_visitors_day.reset_index().rename(columns={'Datetime': 'ds',
                                                                    'Podhura': 'y',
                                                                    'tmax': 'tmax',
                                                                    'tlak': 'tlak',
                                                                    'vlhkost': 'vlhkost'
                                                                    }))
        else:
            model.fit(ts_visitors_day.reset_index().rename(columns={'Datetime': 'ds',
                                                                    'Podhura': 'y',
                                                                    }))
        timeend = time.perf_counter()
        print(f" {timeend - timestart:0.4f} vteřin")
        horizons = len(ts_exog_day_pred)
        future = model.make_future_dataframe(periods=horizons, freq='D')

        if self.weather:
            future['tmax'] = self.timeserie.ts_exog_weather_day['tmax'].values
            future['tlak'] = self.timeserie.ts_exog_weather_day['tlak'].values
            future['vlhkost'] = self.timeserie.ts_exog_weather_day['vlhkost'].values

        fcst = model.predict(future)
        BOXCOX_LAMBDA = 1
        fcst["yhat"] = boxcox(fcst['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcst["yhat_lower"] = fcst['yhat_lower'].clip(lower=0)
        fcst["yhat_upper"] = boxcox(fcst['yhat_upper'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)

        fig = model.plot(fcst)
        plt.show()

        # define the period for which we want a prediction
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date,
                                                      periods=horizons, freq="D")]
        future = DataFrame(datelist_predicted)
        future.columns = ['ds']
        future['ds'] = to_datetime(future['ds'])
        if self.weather:
            future['tmax'] = ts_exog_day_pred['tmax'].values
            future['tlak'] = ts_exog_day_pred['tlak'].values
            future['vlhkost'] = ts_exog_day_pred['vlhkost'].values

        forecast = model.predict(future)

        model.plot_components(forecast)
        plt.title('Složky')
        plt.show()
        forecast["yhat"] = boxcox(forecast['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        forecast["yhat_upper"] = boxcox(forecast['yhat_upper'].clip(lower=0) + 10, lmbda=BOXCOX_LAMBDA)
        forecast["yhat_lower"] = forecast['yhat_lower'].clip(lower=0)
        forecast.index = forecast['ds']
        plt.plot(self.timeserie.ts_visitors_day.tail(horizons * 4)[self.timeserie.park], c='blue', label='Návštěvníci')
        plt.plot(forecast['yhat'], c='green', label='Predikce')
        plt.fill_between(forecast.index,
                         forecast['yhat_lower'],
                         forecast['yhat_upper'],
                         color='k', alpha=.15)
        plt.legend()
        plt.title('Prophet - ' + self.timeserie.park + ' predikce')
        plt.xticks(rotation=20)
        plt.show()

    def predictAll(self):
        ts_visitors_day = self.timeserie.ts_visitors_day.copy()
        ts_visitors_day = self.timeserie.ts_visitors_day.loc[self.timeserie.min_date:self.timeserie.max_date]
        ts_exog_day = self.timeserie.ts_exog_weather_day.loc[
                      self.timeserie.min_date:self.timeserie.max_date - datetime.timedelta(days=1)]
        ts_exog_day_pred = self.timeserie.ts_exog_weather_day.loc[
                           self.timeserie.max_date:self.timeserie.max_predicted_date - datetime.timedelta(days=1)]
        ts_visitors_day.dropna(inplace=True)
        index_forecasted = np.arange(len(ts_visitors_day.values))
        ts_visitors_day['Datum'] = ts_visitors_day.index
        ts_visitors_day['y'] = ts_visitors_day[self.timeserie.park]
        ts_visitors_day.drop(columns=[self.timeserie.park], inplace=True)
        ts_visitors_day.index = index_forecasted
        ts_visitors_day.columns = ['ds', 'y']

        years = pd.DatetimeIndex(ts_visitors_day['ds']).year

        # no exog
        modelNo = Prophet(
            interval_width=0.95,
            seasonality_mode="additive",
            yearly_seasonality=True,
            weekly_seasonality=True,
            daily_seasonality=False
        )

        timestart = time.perf_counter()

        modelNo.fit(ts_visitors_day.reset_index().rename(columns={'Datetime': 'ds',
                                                                  'Podhura': 'y',
                                                                  }))
        timeend = time.perf_counter()
        print(f" {timeend - timestart:0.4f} vteřin")
        horizons = len(ts_exog_day_pred)
        future = modelNo.make_future_dataframe(periods=horizons, freq='D')

        fcstNo = modelNo.predict(future)

        fcstNo = fcstNo[-7:]
        BOXCOX_LAMBDA = 1
        fcstNo["yhat"] = boxcox(fcstNo['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcstNo["yhat_lower"] = fcstNo['yhat_lower'].clip(lower=0)
        fcstNo["yhat_upper"] = boxcox(fcstNo['yhat_upper'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)


        # define the period for which we want a prediction
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date,
                                                      periods=horizons, freq="D")]
        futureNo = DataFrame(datelist_predicted)
        futureNo.columns = ['ds']
        futureNo['ds'] = to_datetime(futureNo['ds'])
        forecastNo = modelNo.predict(future)
        forecastNo = forecastNo[-7:]

        forecastNo["yhat"] = boxcox(forecastNo['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        forecastNo["yhat_upper"] = boxcox(forecastNo['yhat_upper'].clip(lower=0) + 10, lmbda=BOXCOX_LAMBDA)
        forecastNo["yhat_lower"] = forecastNo['yhat_lower'].clip(lower=0)
        forecastNo.index = forecastNo['ds']

        # holidays
        cz_holidays = self.holidays(max(years))
        modelHol = Prophet(
            interval_width=0.95,
            seasonality_mode="additive",
            yearly_seasonality=True,
            weekly_seasonality=True,
            daily_seasonality=False,
            holidays_prior_scale=0.1,
            holidays=cz_holidays
        )

        timestart = time.perf_counter()

        modelHol.fit(ts_visitors_day.reset_index().rename(columns={'Datetime': 'ds',
                                                                   'Podhura': 'y',
                                                                   }))
        timeend = time.perf_counter()
        print(f" {timeend - timestart:0.4f} vteřin")
        horizons = len(ts_exog_day_pred)

        future = modelNo.make_future_dataframe(periods=horizons, freq='D')
        fcstHol = modelHol.predict(future)
        BOXCOX_LAMBDA = 1
        fcstHol["yhat"] = boxcox(fcstHol['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcstHol["yhat_lower"] = fcstHol['yhat_lower'].clip(lower=0)
        fcstHol["yhat_upper"] = boxcox(fcstHol['yhat_upper'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)

        # define the period for which we want a prediction
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date,
                                                      periods=horizons, freq="D")]
        futureHol = DataFrame(datelist_predicted)
        futureHol.columns = ['ds']
        futureHol['ds'] = to_datetime(futureHol['ds'])
        if self.weather:
            futureHol['tmax'] = ts_exog_day_pred['tmax'].values
            futureHol['tlak'] = ts_exog_day_pred['tlak'].values
            futureHol['vlhkost'] = ts_exog_day_pred['vlhkost'].values

        forecastHol = modelHol.predict(futureHol)

        forecastHol["yhat"] = boxcox(forecastHol['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        forecastHol["yhat_upper"] = boxcox(forecastHol['yhat_upper'].clip(lower=0) + 10, lmbda=BOXCOX_LAMBDA)
        forecastHol["yhat_lower"] = forecastHol['yhat_lower'].clip(lower=0)
        forecastHol.index = forecastHol['ds']

        pd.options.mode.chained_assignment = None
        ts_visitors_day.loc[:, 'tmax'] = ts_exog_day.loc[:, 'tmax'].values
        ts_visitors_day.loc[:, 'tlak'] = ts_exog_day.loc[:, 'tlak'].values
        ts_visitors_day.loc[:, 'vlhkost'] = ts_exog_day.loc[:, 'vlhkost'].values

        # all
        modelAll = Prophet(
            interval_width=0.95,
            seasonality_mode="additive",
            yearly_seasonality=True,
            weekly_seasonality=True,
            daily_seasonality=False
        )

        modelAll.add_regressor('tmax')
        modelAll.add_regressor('tlak')
        modelAll.add_regressor('vlhkost')
        # fit the model
        timestart = time.perf_counter()
        modelAll.fit(ts_visitors_day.reset_index().rename(columns={'Datetime': 'ds',
                                                                   'Podhura': 'y',
                                                                   'tmax': 'tmax',
                                                                   'tlak': 'tlak',
                                                                   'vlhkost': 'vlhkost'
                                                                   }))
        timeend = time.perf_counter()
        print(f" {timeend - timestart:0.4f} vteřin")
        horizons = len(ts_exog_day_pred)
        futureAll = modelAll.make_future_dataframe(periods=horizons, freq='D')

        futureAll['tmax'] = self.timeserie.ts_exog_weather_day['tmax'].values
        futureAll['tlak'] = self.timeserie.ts_exog_weather_day['tlak'].values
        futureAll['vlhkost'] = self.timeserie.ts_exog_weather_day['vlhkost'].values

        fcstAll = modelAll.predict(futureAll)
        BOXCOX_LAMBDA = 1
        fcstAll["yhat"] = boxcox(fcstAll['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        fcstAll["yhat_lower"] = fcstAll['yhat_lower'].clip(lower=0)
        fcstAll["yhat_upper"] = boxcox(fcstAll['yhat_upper'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)

        # define the period for which we want a prediction
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date,
                                                      periods=horizons, freq="D")]
        futureAll = DataFrame(datelist_predicted)
        futureAll.columns = ['ds']
        futureAll['ds'] = to_datetime(futureAll['ds'])
        futureAll['tmax'] = ts_exog_day_pred['tmax'].values
        futureAll['tlak'] = ts_exog_day_pred['tlak'].values
        futureAll['vlhkost'] = ts_exog_day_pred['vlhkost'].values

        forecastAll = modelAll.predict(futureAll)

        forecastAll["yhat"] = boxcox(forecastAll['yhat'].clip(lower=0) + 1, lmbda=BOXCOX_LAMBDA)
        forecastAll["yhat_upper"] = boxcox(forecastAll['yhat_upper'].clip(lower=0) + 10, lmbda=BOXCOX_LAMBDA)
        forecastAll["yhat_lower"] = forecastAll['yhat_lower'].clip(lower=0)
        forecastAll.index = forecastAll['ds']

        plt.plot(self.timeserie.ts_visitors_day.tail(horizons * 4)[self.timeserie.park], c='blue', label='Návštěvníci')
        plt.plot(forecastNo['yhat'], c='green', label='Bez exog')
        plt.fill_between(forecastNo.index,
                         forecastNo['yhat_lower'],
                         forecastNo['yhat_upper'],
                         color='k', alpha=.15)
        plt.plot(forecastHol['yhat'], c='yellow', label='Predikce Svátek')
        plt.fill_between(forecastHol.index,
                         forecastHol['yhat_lower'],
                         forecastHol['yhat_upper'],
                         color='k', alpha=.15)
        plt.plot(forecastAll['yhat'], c='red', label='Všechny exog')
        plt.fill_between(forecastAll.index,
                         forecastAll['yhat_lower'],
                         forecastAll['yhat_upper'],
                         color='k', alpha=.15)
        plt.legend()
        plt.title('Prophet - ' + self.timeserie.park + ' predikce')
        plt.xticks(rotation=20)
        plt.show()
