# This is a sample Python script.
# Import libraries
import pandas as pd

from forecasting.model.fbprophet import Fbprophet
from forecasting.model.park_timeserie import Park_timeserie
from forecasting.model.sarimax import Sarimax


def arima(park_timeserie):
    # SARIMAX DAY
    arima_exog_holiday = input("Do you want to add holidays as exogenous variables? (Y / N):\n")
    arima_exog_holiday = arima_exog_holiday.upper()
    if arima_exog_holiday == "Y":
        arima_exog_holiday = True
    else:
        arima_exog_holiday = False

    arima_exog_weather = input("Do you want to add weather as exogenous variables? (Y / N):\n")
    arima_exog_weather = arima_exog_weather.upper()
    if arima_exog_weather == "Y":
        park_timeserie.showExog()
        arima_exog_weather = True
    else:
        arima_exog_weather = False

    sarimax = Sarimax(timeserie=park_timeserie, name="SARIMAX", holiday=arima_exog_holiday, weather=arima_exog_weather)
    sarimax.predict()
    sarimax.predict_test(7)
    if arima_exog_weather == True and arima_exog_holiday == True:
        sarimax.predictAll()


def fbprophet(park_timeserie):
    fb_exog_holiday = input("Do you want to add holidays as exogenous variables? (Y / N):\n")
    fb_exog_holiday = fb_exog_holiday.upper()
    if fb_exog_holiday == "Y":
        fb_exog_holiday = True
    else:
        fb_exog_holiday = False

    fb_exog_weather = input("Do you want to add weather as exogenous variables? (Y / N):\n")
    fb_exog_weather = fb_exog_weather.upper()
    if fb_exog_weather == "Y":
        fb_exog_weather = True
        park_timeserie.showExog()
    else:
        fb_exog_weather = False

    fbprophet1 = Fbprophet(name="Fbprophet", timeserie=park_timeserie, exogenous_var=fb_exog_weather,
                           holiday=fb_exog_holiday, weather=fb_exog_weather)
    fbprophet1.predict_test()
    fbprophet1.predict()
    if fb_exog_holiday == True and fb_exog_weather == True:
        fbprophet1.predictAll()


def prepareData():
    while True:
        park = input("Enter name one of this parks (Oheb, Silink, Podhura, Mastale, Stezka):\n")
        park = park.capitalize()
        file = 'Data_ZH_19-20_1.xlsx'
        sheet = 'Data_ZH_19-20'
        if park == "Oheb" or park == "O":
            dataset_hours_all = pd.read_excel(io=file, sheet_name=sheet, header=0,
                                              parse_dates=[0], index_col=0, usecols="A,B,I,O,R,S,W")
            break
        elif park == "Silink":
            dataset_hours_all = pd.read_excel(io=file, sheet_name=sheet, header=0,
                                              parse_dates=[0], index_col=0, usecols="A,B,J,O,R,S,W")
            break
        elif park == "Podhura":
            dataset_hours_all = pd.read_excel(io=file, sheet_name=sheet, header=0,
                                              parse_dates=[0], index_col=0, usecols="A,B,K,O,R,S,W")
            break
        elif park == "Mastale":
            dataset_hours_all = pd.read_excel(io=file, sheet_name=sheet, header=0,
                                              parse_dates=[0], index_col=0, usecols="A,B,L,O,R,S,W")
            break
        elif park == "Stezka":
            dataset_hours_all = pd.read_excel(io=file, sheet_name=sheet, header=0,
                                              parse_dates=[0], index_col=0, usecols="A,B,M,O,R,S,W")
            break
        else:
            print("Repeat the name of park.")

    show_graphs = False

    timeserie = Park_timeserie(park, dataset_hours_all, show_graphs)
    timeserie.prepareExog()
    timeserie.timeseriesDates()

    timeserie.years()
    timeserie.seasonalDecompose()
    timeserie.adfTest()
    timeserie.testStationarity()

    if show_graphs:
        timeserie.showExog()
        timeserie.correlation()

    return timeserie


# Start
model = ""
park = ''
dataset_hours_all = ''
exog_hours = ''
while True:
    model = input("Choose from models (ARIMA, FB - FBPROPHET):\n")
    model = model.upper()
    if model == "ARIMA":
        park_timeserie = prepareData()
        park_timeserie.displayTimeserie()
        arima(park_timeserie)
        break
    elif model == "FBProphet" or model == "FB":
        park_timeserie = prepareData()
        park_timeserie.displayTimeserie()
        fbprophet(park_timeserie)
        break
    else:
        print("Repeat the name of model.")
