import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pmdarima import auto_arima
from pmdarima.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, mean_absolute_error


class Sarimax:

    def __init__(self, timeserie, name, holiday, weather):
        self.timeserie = timeserie
        self.name = name
        self.holiday = holiday
        self.weather = weather
        if holiday or weather:
            self.timeserie.ts_exog_weather_day = self.timeserie.ts_exog_weather_day.merge(
                self.timeserie.ts_exog_holidays_day, left_index=True, right_index=True)

    def errors(self, forecast, test):
        mse = mean_squared_error(y_true=test,
                                 y_pred=forecast)
        print("MSE: ", mse)
        mae = mean_absolute_error(y_true=test,
                                  y_pred=forecast)
        print("MAE: ", mae)

    def prepare_predict_test(self, forecaststt, confintt, train, test, horizons_test):
        # Visualize the forecasts (orange=train, green=forecasts)
        forecasted_series_test = pd.DataFrame(forecaststt, index=test.index, columns=[self.timeserie.park])
        forecasted_series_test.index = pd.to_datetime(forecasted_series_test.index)

        # lower and upper series
        lower_series = pd.Series(confintt[:, 0].tolist(), index=test.index)
        lower_series.index = test.index
        lower_series.index = pd.to_datetime(lower_series.index)
        upper_series = pd.Series(confintt[:, 1].tolist(), index=test.index)
        lower_series.index = test.index
        upper_series.index = pd.to_datetime(upper_series.index)
        lower_series[lower_series.values < 0] = 0
        upper_series[upper_series.values < 0] = 0

        plt.plot(train[self.timeserie.park][-(horizons_test * 4):], c='blue', label='Trénovací data')
        plt.plot(test[self.timeserie.park], c='orange', label='Testovací data')
        forecasted_series_test[forecasted_series_test.values < 0] = 0
        plt.plot(forecasted_series_test.tail(horizons_test)[self.timeserie.park], c='green', label='Predikce')
        plt.fill_between(lower_series.index,
                         lower_series,
                         upper_series,
                         color='k', alpha=.15)
        plt.legend()
        plt.title('ARIMA - ' + self.timeserie.park + ' test predikce')
        plt.xticks(rotation=20)
        plt.show()
        return forecasted_series_test

    def prepare_predict(self, forecasts, confint, horizons):
        index_forecasted = np.arange(len(self.timeserie.ts_visitors_day.values),
                                     len(self.timeserie.ts_visitors_day.values) + horizons)
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date + datetime.timedelta(days=1),
                                                      periods=horizons, freq="D")]

        # Visualize the forecasts
        forecasted_series = pd.DataFrame({self.timeserie.park: forecasts, "Datum": datelist_predicted},
                                         index=index_forecasted)
        forecasted_series = forecasted_series.set_index(forecasted_series.Datum)
        forecasted_series.drop(columns=["Datum"], inplace=True)
        forecasted_series.index = pd.to_datetime(forecasted_series.index)
        forecasted_series[forecasted_series.values < 0] = 0

        # edit lower and upper series
        lower_series = pd.Series(confint[:, 0].tolist(), index=index_forecasted)
        lower_series.index = datelist_predicted
        lower_series.index = pd.to_datetime(lower_series.index)
        lower_series.columns = ['Date', 'conf']
        upper_series = pd.Series(confint[:, 1].tolist(), index=index_forecasted)
        upper_series.index = datelist_predicted
        upper_series.index = pd.to_datetime(upper_series.index)

        lower_series[lower_series.values < 0] = 0
        upper_series[upper_series.values < 0] = 0

        plt.plot(self.timeserie.ts_visitors_day.tail(horizons * 4)[self.timeserie.park], c='blue', label='Návštěvníci')
        plt.plot(forecasted_series.tail(horizons)[self.timeserie.park], c='green', antialiased=True, label='Predikce')
        plt.fill_between(lower_series.index,
                         lower_series,
                         upper_series,
                         color='k', alpha=.15)
        plt.xticks(rotation=20)
        plt.title('ARIMA - ' + self.timeserie.park + ' predikce')
        plt.legend()
        plt.show()

    def predict_test(self, horizons_test):
        if self.weather or self.holiday:
            exog_days_1 = self.timeserie.ts_exog_weather_day.loc[
                          self.timeserie.min_date - datetime.timedelta(
                              days=1):self.timeserie.max_date - datetime.timedelta(
                              days=1)]
            if self.weather and self.holiday == False:
                exog_days_1 = exog_days_1["tmax", "tlak", "vlhkost"]
            if self.weather == False and self.holiday:
                exog_days_1 = exog_days_1["svatek"]

        # train test
        # horizons_test = 7
        train, test = train_test_split(self.timeserie.ts_visitors_day, test_size=horizons_test)
        train_exog, test_exog = '', ''
        if self.weather or self.holiday:
            train_exog, test_exog = train_test_split(exog_days_1, test_size=horizons_test)
            model = auto_arima(train,
                               seasonal=True,
                               m=7,
                               suppress_warnings=True,
                               trace=3,
                               exogenous=train_exog
                               )
        else:
            model = auto_arima(train,
                               seasonal=True,
                               m=7,
                               suppress_warnings=True,
                               trace=3,
                               stepwise=True
                               )
        print(model.summary())
        print(f'best_aic: {model.aic()}')
        print(f'best_bic: {model.bic()}')

        if self.weather:
            modeltt = model.fit(y=train, exogenous=train_exog)
        else:
            modeltt = model.fit(y=train)

        modeltt.plot_diagnostics(figsize=(10, 8))
        plt.show()

        # make your forecasts
        if self.weather:
            forecaststt, confintt = modeltt.predict(n_periods=horizons_test,
                                                    exogenous=test_exog,
                                                    return_conf_int=True, alpha=0.05)
        else:
            forecaststt, confintt = modeltt.predict(n_periods=horizons_test,
                                                    return_conf_int=True, alpha=0.05)

        forecasted_series_test = self.prepare_predict_test(forecaststt, confintt, train, test, horizons_test)

        y_true = self.timeserie.ts_visitors_day[self.timeserie.park][-horizons_test:].values
        y_pred = forecasted_series_test.values
        self.errors(y_pred, y_true)

    def predict(self):
        ts_visitors_day = self.timeserie.ts_visitors_day.loc[self.timeserie.min_date:self.timeserie.max_date]
        ts_visitors_day.dropna(inplace=True)
        horizons = 7
        if self.weather or self.holiday:
            ts_exog_pred = self.timeserie.ts_exog_weather_day.loc[
                           self.timeserie.max_date:self.timeserie.max_predicted_date]

            ts_exog_day = self.timeserie.ts_exog_weather_day.loc[
                          self.timeserie.min_date:self.timeserie.max_date - datetime.timedelta(days=1)]
            if self.weather and self.holiday == False:
                ts_exog_day = ts_exog_day["tmax", "tlak", "vlhkost"]
                ts_exog_pred = ts_exog_pred["tmax", "tlak", "vlhkost"]
            if self.weather == False and self.holiday:
                ts_exog_day = ts_exog_day["svatek"]
                ts_exog_pred = ts_exog_pred["svatek"]

        if self.weather or self.holiday:
            model = auto_arima(ts_visitors_day,
                               seasonal=True,
                               m=7,
                               suppress_warnings=True,
                               trace=3,
                               exogenous=ts_exog_day
                               )
        else:
            model = auto_arima(ts_visitors_day,
                               seasonal=True,
                               m=7,
                               suppress_warnings=True,
                               trace=3,
                               )

        print(model.summary())
        print(f'best_aic: {model.aic()}')
        print(f'best_bic: {model.bic()}')

        if self.weather or self.holiday:
            results = model.fit(y=ts_visitors_day, exogenous=ts_exog_day)
        else:
            results = model.fit(y=ts_visitors_day)

        results.plot_diagnostics(figsize=(10, 8))
        plt.show()

        # make forecasts
        if self.weather or self.holiday:
            horizons = len(ts_exog_pred)
            forecasts, confint = results.predict(n_periods=horizons,
                                                 exogenous=ts_exog_pred,
                                                 return_conf_int=True, alpha=0.05)
        else:
            forecasts, confint = results.predict(n_periods=horizons,
                                                 return_conf_int=True, alpha=0.05)

        self.prepare_predict(forecasts, confint, horizons)

    def predictAll(self):
        ts_visitors_day = self.timeserie.ts_visitors_day.loc[self.timeserie.min_date:self.timeserie.max_date]
        ts_visitors_day.dropna(inplace=True)
        horizons = 7

        model = auto_arima(ts_visitors_day,
                            seasonal=True,
                            m=7,
                            suppress_warnings=True,
                            trace=3,
                            )

        print(model.summary())
        print(f'best_aic: {model.aic()}')
        print(f'best_bic: {model.bic()}')

        results = model.fit(y=ts_visitors_day)

        forecasts, confint = results.predict(n_periods=horizons,
                                                 return_conf_int=True, alpha=0.05)

        index_forecasted = np.arange(len(self.timeserie.ts_visitors_day.values),
                                     len(self.timeserie.ts_visitors_day.values) + horizons)
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date + datetime.timedelta(days=1),
                                                      periods=horizons, freq="D")]

        # Visualize the forecasts
        forecasted_seriesNo = pd.DataFrame({self.timeserie.park: forecasts, "Datum": datelist_predicted},
                                         index=index_forecasted)
        forecasted_seriesNo = forecasted_seriesNo.set_index(forecasted_seriesNo.Datum)
        forecasted_seriesNo.drop(columns=["Datum"], inplace=True)
        forecasted_seriesNo.index = pd.to_datetime(forecasted_seriesNo.index)
        forecasted_seriesNo[forecasted_seriesNo.values < 0] = 0

        # edit lower and upper series
        lower_seriesNo = pd.Series(confint[:, 0].tolist(), index=index_forecasted)
        lower_seriesNo.index = datelist_predicted
        lower_seriesNo.index = pd.to_datetime(lower_seriesNo.index)
        lower_seriesNo.columns = ['Date', 'conf']
        upper_seriesNo = pd.Series(confint[:, 1].tolist(), index=index_forecasted)
        upper_seriesNo.index = datelist_predicted
        upper_seriesNo.index = pd.to_datetime(upper_seriesNo.index)

        lower_seriesNo[lower_seriesNo.values < 0] = 0
        upper_seriesNo[upper_seriesNo.values < 0] = 0

        #holidays
        ts_exog_pred = self.timeserie.ts_exog_weather_day.loc[
                      self.timeserie.max_date:self.timeserie.max_predicted_date]

        ts_exog_day = self.timeserie.ts_exog_weather_day.loc[
                      self.timeserie.min_date:self.timeserie.max_date - datetime.timedelta(days=1)]

        ts_exog_day = ts_exog_day["svatek"]
        ts_exog_pred = ts_exog_pred["svatek"]

        model = auto_arima(ts_visitors_day,
                               seasonal=True,
                               m=7,
                               suppress_warnings=True,
                               trace=3,
                               exogenous=ts_exog_day
                               )

        print(model.summary())
        print(f'best_aic: {model.aic()}')
        print(f'best_bic: {model.bic()}')

        results = model.fit(y=ts_visitors_day, exogenous=ts_exog_day)

        # make forecasts
        horizons = len(ts_exog_pred)
        forecasts, confint = results.predict(n_periods=horizons,
                                                 exogenous=ts_exog_pred,
                                                 return_conf_int=True, alpha=0.05)

        index_forecasted = np.arange(len(self.timeserie.ts_visitors_day.values),
                                     len(self.timeserie.ts_visitors_day.values) + horizons)
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date + datetime.timedelta(days=1),
                                                      periods=horizons, freq="D")]

        # Visualize the forecasts
        forecasted_seriesHol = pd.DataFrame({self.timeserie.park: forecasts, "Datum": datelist_predicted},
                                           index=index_forecasted)
        forecasted_seriesHol = forecasted_seriesHol.set_index(forecasted_seriesHol.Datum)
        forecasted_seriesHol.drop(columns=["Datum"], inplace=True)
        forecasted_seriesHol.index = pd.to_datetime(forecasted_seriesHol.index)
        forecasted_seriesHol[forecasted_seriesHol.values < 0] = 0

        # edit lower and upper series
        lower_seriesHol = pd.Series(confint[:, 0].tolist(), index=index_forecasted)
        lower_seriesHol.index = datelist_predicted
        lower_seriesHol.index = pd.to_datetime(lower_seriesHol.index)
        lower_seriesHol.columns = ['Date', 'conf']
        upper_seriesHol = pd.Series(confint[:, 1].tolist(), index=index_forecasted)
        upper_seriesHol.index = datelist_predicted
        upper_seriesHol.index = pd.to_datetime(upper_seriesHol.index)

        lower_seriesHol[lower_seriesHol.values < 0] = 0
        upper_seriesHol[upper_seriesHol.values < 0] = 0

        #all
        ts_exog_pred = self.timeserie.ts_exog_weather_day.loc[
                        self.timeserie.max_date:self.timeserie.max_predicted_date]
        ts_exog_day = self.timeserie.ts_exog_weather_day.loc[
                          self.timeserie.min_date:self.timeserie.max_date - datetime.timedelta(days=1)]

        model = auto_arima(ts_visitors_day,
                               seasonal=True,
                               m=7,
                               suppress_warnings=True,
                               trace=3,
                               exogenous=ts_exog_day
                               )

        print(model.summary())
        print(f'best_aic: {model.aic()}')
        print(f'best_bic: {model.bic()}')

        results = model.fit(y=ts_visitors_day, exogenous=ts_exog_day)

        # make forecasts
        horizons = len(ts_exog_pred)
        forecasts, confint = results.predict(n_periods=horizons,
                                                 exogenous=ts_exog_pred,
                                                 return_conf_int=True, alpha=0.05)

        index_forecasted = np.arange(len(self.timeserie.ts_visitors_day.values),
                                     len(self.timeserie.ts_visitors_day.values) + horizons)
        datelist_predicted = [d.strftime('%Y-%m-%d %A')
                              for d in pd.bdate_range(start=self.timeserie.max_date + datetime.timedelta(days=1),
                                                      periods=horizons, freq="D")]

        # Visualize the forecasts
        forecasted_seriesAll = pd.DataFrame({self.timeserie.park: forecasts, "Datum": datelist_predicted},
                                            index=index_forecasted)
        forecasted_seriesAll = forecasted_seriesAll.set_index(forecasted_seriesAll.Datum)
        forecasted_seriesAll.drop(columns=["Datum"], inplace=True)
        forecasted_seriesAll.index = pd.to_datetime(forecasted_seriesAll.index)
        forecasted_seriesAll[forecasted_seriesAll.values < 0] = 0

        # edit lower and upper series
        lower_seriesAll = pd.Series(confint[:, 0].tolist(), index=index_forecasted)
        lower_seriesAll.index = datelist_predicted
        lower_seriesAll.index = pd.to_datetime(lower_seriesAll.index)
        lower_seriesAll.columns = ['Date', 'conf']
        upper_seriesAll = pd.Series(confint[:, 1].tolist(), index=index_forecasted)
        upper_seriesAll.index = datelist_predicted
        upper_seriesAll.index = pd.to_datetime(upper_seriesAll.index)

        lower_seriesAll[lower_seriesAll.values < 0] = 0
        upper_seriesAll[upper_seriesAll.values < 0] = 0

        plt.plot(self.timeserie.ts_visitors_day.tail(horizons * 4)[self.timeserie.park], c='blue', label='Návštěvníci')
        plt.plot(forecasted_seriesNo.tail(horizons)[self.timeserie.park], c='green', antialiased=True, label='Bez exog')
        plt.fill_between(lower_seriesNo.index,
                         lower_seriesNo,
                         upper_seriesNo,
                         color='k', alpha=.15)
        plt.plot(forecasted_seriesHol.tail(horizons)[self.timeserie.park], c='yellow', antialiased=True, label='Svatek')
        plt.fill_between(lower_seriesHol.index,
                         lower_seriesHol,
                         upper_seriesHol,
                         color='k', alpha=.15)
        plt.plot(forecasted_seriesAll.tail(horizons)[self.timeserie.park], c='red', antialiased=True, label='Všechny exog')
        plt.fill_between(lower_seriesAll.index,
                         lower_seriesAll,
                         upper_seriesAll,
                         color='k', alpha=.15)

        plt.xticks(rotation=20)
        plt.title('ARIMA - ' + self.timeserie.park + ' predikce')
        plt.legend()
        plt.show()
