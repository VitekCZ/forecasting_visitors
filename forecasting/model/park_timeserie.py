import pandas as pd
import seaborn as sns
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.stattools import adfuller
import matplotlib.pyplot as plt
import numpy as np
import datetime


class Park_timeserie:
    ts_visitors = ''
    ts_exog_all = ''
    ts_exog = ''
    ts_visitors_day = ''
    ts_exog_weather_day = ''
    min_date = ''
    max_date = ''
    max_predicted_date = ''
    ts_exog_holidays_day = ''
    stationarity = False

    def __init__(self, park, ts_original, show_graphs):
        self.park = park
        ts_original.index = pd.DatetimeIndex(ts_original.index)
        self.ts_original = ts_original
        self.show_graphs = show_graphs

        # data visitors hours
        self.ts_visitors = ts_original.copy()
        self.ts_visitors.drop(columns=["tmax", "tlak", "vlhkost", "svatek"], inplace=True)
        # clear empty values
        self.ts_visitors.dropna(inplace=True)

        # day data
        self.ts_visitors_day = self.ts_visitors.copy()
        self.ts_visitors_day = pd.DataFrame(self.ts_visitors_day.groupby(self.ts_visitors_day.Datum).sum())
        self.ts_visitors.drop(columns=["Datum"], inplace=True)

        print(self.ts_visitors_day.describe())
        self.ts_visitors_day[park] += 1
        if self.park == "Other":
            plt.plot(self.ts_visitors_day["Stezka"], label="Stezka")
            plt.plot(self.ts_visitors_day["Oheb"], label="Oheb")
            plt.plot(self.ts_visitors_day["Podhura"], label="Podhůra")
            plt.plot(self.ts_visitors_day["Mastale"], label="Maštale")
            plt.plot(self.ts_visitors_day["Silink"], label="Šilink")
            plt.title("Oblasti")
            plt.legend()
            plt.show()

        # dates
        self.min_date = min(self.ts_original.index) - pd.DateOffset(hours=9)
        self.max_date = self.ts_visitors.index.max() + pd.DateOffset(hours=1)
        self.max_predicted_date = self.ts_original.index.max() + pd.DateOffset(hours=1)

        self.ts_visitors_day = self.ts_visitors_day.loc[
                               self.min_date - datetime.timedelta(days=1):self.max_date - datetime.timedelta(days=1)]
        plt.figure(figsize=(14, 8), dpi=80)
        plt.plot(self.ts_visitors_day)
        plt.title(self.park)
        plt.ylabel('Návštěvníci')
        plt.show()

    def timeseriesDates(self):
        print(self.min_date)
        print(self.max_date)
        print(self.max_predicted_date)

    def prepareExog(self):
        # exogenous variables
        self.ts_exog_all = self.ts_original.copy()

        self.ts_exog = self.ts_exog_all.copy()
        self.ts_exog.dropna(inplace=True)
        self.ts_exog_all.drop(columns=[self.park], inplace=True)
        self.ts_exog.drop(columns=[self.park], inplace=True)

        self.ts_exog_weather_day = self.ts_exog_all.copy()
        self.ts_exog_weather_day = self.ts_exog_weather_day.groupby(self.ts_exog_weather_day.Datum).agg(
            {'tmax': ['mean'], 'tlak': ['mean'], 'vlhkost': ['mean']})

        self.ts_exog_all.drop(columns=["Datum"], inplace=True)
        self.ts_exog.drop(columns=["Datum"], inplace=True)
        # print(self.ts_exog_day)
        print(self.ts_exog.describe())

        # exogenous variables holydays
        ts_exog_holydays = self.ts_original.copy()
        ts_exog_holydays = ts_exog_holydays.copy()
        ts_exog_holydays.drop(columns=[self.park], inplace=True)

        self.ts_exog_holidays_day = ts_exog_holydays.copy()
        self.ts_exog_holidays_day = self.ts_exog_holidays_day.groupby(self.ts_exog_holidays_day.Datum).agg(
            {'svatek': ['mean']})

    def years(self):
        years = self.ts_visitors_day.index.year.unique()
        copyday = self.ts_visitors_day
        copyday['day'] = [d.strftime('%a') for d in copyday.index]
        copyday['month'] = [d.strftime('%b') for d in copyday.index]
        print(copyday['month'])

        # Draw Plot
        day_names = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
        sns.boxplot(x='day', y=self.park, data=copyday, order=day_names)
        plt.title('Denní Box Plot\n(Trend)', fontsize=18);
        plt.show()

        month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        sns.boxplot(x='month', y=self.park, data=copyday, order=month_names)
        plt.title('Měsíční Box Plot\n(Sezónnost)', fontsize=18)
        plt.show()

        self.ts_visitors_day.drop(columns=["day"], inplace=True)
        self.ts_visitors_day.drop(columns=["month"], inplace=True)

    def adfTest(self):
        # p value
        adf = adfuller(self.ts_visitors_day[self.ts_visitors_day[self.park] > 0][self.park])
        print('ADF statisika: %f' % adf[0])
        print('p-hodnota: %f' % adf[1])

    def testStationarity(self):
        # Perform Dickey-Fuller test:
        print('Výsledek Dickey-Fuller Test:')
        dftest = adfuller(self.ts_visitors_day, autolag='AIC')
        dfoutput = pd.Series(dftest[0:4],
                             index=['Test statistika', 'p-hodnota', '#počet zpoždění', 'Number of Observations Used'])
        for key, value in dftest[4].items():
            dfoutput['Critical Value (%s)' % key] = value
        print(dfoutput)
        if dfoutput['p-hodnota'] > 0.05:
            print("Stacionární")
            self.stationarity = True
            return True
        else:
            print("Nestacionární")
            self.stationarity = False
            return False

    def correlation(self):
        # ACF
        plot_acf(self.ts_visitors_day, title="Autokorelace", lags=50)
        plt.show()
        # PACF
        plot_pacf(self.ts_visitors_day, title="Parciální autokorelace", lags=50)
        plt.show()
        diff_dataset_days = self.ts_visitors_day.diff(12)
        diff_dataset_days.dropna()

    def differencing(self):
        self.ts_visitors_day.loc[(self.ts_visitors_day != 0).any(axis=1)]
        ts_log = np.log(self.ts_visitors_day)
        # 1st Differencing
        diff1 = ts_log.diff()
        plt.plot(diff1)
        plt.title("1st Differencing")
        print(f"Mean for difference d order 1: {np.mean(diff1)}")
        print(f"Variance for difference d order 1: {np.var(diff1)}")
        plt.show()
        # 2nd Differencing
        diff2 = diff1.diff()
        plt.plot(ts_log.diff().diff())
        plt.title("2nd Differencing")
        print(f"Mean for difference d order 2: {np.mean(diff2)}")
        print(f"Variance for difference d order 2: {np.var(diff2)}")
        plt.show()

    def displayTimeserie(self):
        print("Jméno: ", self.park)
        print(self.ts_visitors)

    def showExog(self):
        # exogenous variables plot
        plt.plot(self.ts_exog_weather_day.loc[self.max_date - datetime.timedelta(days=28):self.max_predicted_date].tmax,
                 'r', label="tmax")
        plt.plot(self.ts_visitors_day.loc[self.max_date - datetime.timedelta(days=28):self.max_predicted_date][self.park],
                 'b', label="návštěvnost")
        plt.title("Maximální teplota a návštěvnost v posledních 3 týdnech")
        plt.legend()
        plt.xticks(rotation=20)
        plt.show()
        plt.plot(self.ts_exog_weather_day.loc[self.max_date - datetime.timedelta(days=28):self.max_predicted_date].tlak,
                 'm', label="tlak")
        plt.plot(
            self.ts_visitors_day.loc[self.max_date - datetime.timedelta(days=28):self.max_predicted_date][self.park],
            'b', label="návštěvnost")
        plt.title("Tlak a návštěvnost v posledních 3 týdnech")
        plt.legend()
        plt.xticks(rotation=20)
        plt.show()
        plt.plot(
            self.ts_exog_weather_day.loc[self.max_date - datetime.timedelta(days=28):self.max_predicted_date].vlhkost,
            'y', label="vlhkost")
        plt.plot(
            self.ts_visitors_day.loc[self.max_date - datetime.timedelta(days=28):self.max_predicted_date][self.park],
            'b', label="návštěvnost")
        plt.title("Vlhkost a návštěvnost v posledních 3 týdnech")
        plt.legend()
        plt.xticks(rotation=20)
        plt.show()

    def showExogLastWeek(self):
        # exogenous variables last week and predicted days
        plt.figure(figsize=(15, 8))
        plt.plot(self.ts_exog_weather_day.loc[self.max_date - datetime.timedelta(days=14):self.max_date].tmax, 'r',
                 label="tmax")
        plt.plot(self.ts_exog_weather_day.loc[self.max_date - datetime.timedelta(days=14):self.max_date].srazky, 'y',
                 label="tlak")
        plt.plot(self.ts_exog_weather_day.loc[self.max_date - datetime.timedelta(days=14):self.max_date].srazky, 'm',
                 label="vlhkost")
        plt.plot(self.ts_exog_weather_day.loc[self.max_date:self.max_predicted_date].tmax, 'r', label="tmax")
        plt.plot(self.ts_exog_weather_day.loc[self.max_date:self.max_predicted_date].srazky, 'y', label="tlak")
        plt.plot(self.ts_exog_weather_day.loc[self.max_date:self.max_predicted_date].srazky, 'm', label="vlhkost")
        plt.title("Exogenni promenne dny")
        plt.legend()
        plt.show()

    def seasonalDecompose(self):
        # seasonal decompose
        decomposition = seasonal_decompose(self.ts_visitors_day[self.park], model='additive', period=12)
        decomposition.plot()
        plt.show()
